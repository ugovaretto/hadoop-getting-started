# wccl

Hadoop/parkour/clojure wordcount example

## Issues

To avoid issues when uncompressing jar into /var/*

    zip -d wccl-0.1.0-SNAPSHOT-standalone.jar META-INF/license/LICENSE*
    zip -d wccl-0.1.0-SNAPSHOT-standalone.jar META-INF/license
    
## Usage

    hadoop jar wccl-0.1.0-SNAPSHOT-standalone.jar input > output
