(defproject wccl "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 [com.damballa/parkour "0.6.2"]
                 [org.apache.hadoop/hadoop-client "2.7.0"]] 
  :main ^:skip-aot wccl.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :user {:dependencies 
                    [[org.clojure/tools.nrepl "0.2.7"]]
                    :plugins 
                    [[cider/cider-nrepl 
                      "0.9.0-SNAPSHOT"] 
                     [jonase/eastwood "0.2.1"]]}})
