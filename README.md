HADOOP/MAVEN HOWTO
==================


These notes explain how to get started with Hadoop from scratch.
To gain an understanding of the basic concepts do got through the lessons in the folowing course:

https://www.udacity.com/course/intro-to-hadoop-and-mapreduce--ud617

The streaming extensions (standard input/output used to pass data around) are used together with Python, which makes things way easier than
starting with a JVM language.


Maven
-----

`Maven` is a build and dependency management system for java projects. Dependencies are
automatcally downloaded as needed during the build process.

https://maven.apache.org/


Create a new project:

`mvn archetype:generate -DgroupId=me.ugovaretto -DartifactId=wordcount -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`

Maven creates the following directory structure:

    project-name
      pom.xml
      src/
        main/
          java/
        test/
          java/


Add java code under `main/java` and `test/java`.

Use `mvn compile` to download dependencies and compile or `mvn install` to build everything
and store the built product into the `target/` directory.

Hadoop
------

Java-based Map-Reduce implementation, original Google paper is here: http://research.google.com/archive/mapreduce.html
also saved into the `resources` directory.

Hadoop implementes the Map-Reduce algorithms and can perform r/w operations on the standard or HDFS based filesystem.

Standard workflow:
1. copy data into hdfs (slow): data are subdivided in chunks and copied to individual nodes
2. launch map-reduce task: processes read data from the node they run on

Issues:
* default is to split text files on EOL boundaries, no support for structures/binay files;
* no support for parallel I/O i.e. copy operations into HDFS are serial and slow
* ideally it should be possible to read from a parallel distributed file system through parallel I/O libraries and avoid HDFS entirely
* http://stackoverflow.com/questions/19570660/hadoop-put-performance-large-file-20gb
* benchnmarks do not take into account data import time!
* it is possible to achieve better performance with standard MPI, however the main issue in this case is that data must be sorted before being passed to the mapping processes
  i.e. all sorting reduction operations must be completed before the reducers are invoked 

Docs: https://hadoop.apache.org/docs/

In order to build Hadoop based applications with Maven you need to include the proper libaries.

Add to `pom.xml` the proper repository, I'm using Cloudera.

    <repositories>
      <repository>
        <id>cloudera</id>
        <url>https://repository.cloudera.com/artifactory/cloudera-repos/</url>
      </repository>
    </repositories>

Also add the dependency on Hadoop:

    ... in <dependencies> section
    <dependency>
      <groupId>org.apache.hadoop</groupId>
      <artifactId>hadoop-client</artifactId>
      <version>2.7.0</version>
    </dependency>
    ...


If you want you can also use an assembly descriptor to tell maven what has to go into
the final product: in this case create an `assembly.xml` file and reference it from
`pom.xml`

e.g.

    <build>
        <plugins>
          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <version>2.4</version>
            <executions>
              <execution>
                <id>distro-assembly</id>
                <phase>package</phase>
                <goals>
                  <goal>single</goal>
                </goals>
                <configuration>
                  <descriptors>
                    <descriptor>assembly.xml</descriptor>
                  </descriptors>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
    </build>



Running - updated
-----------------

Do follow the instructions on this page:

https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/SingleCluster.html

Or keep on reading to use the WordCount example built according to the instructions above.

Configuration files:

    $HADOOP_HOME/etc/hadoop/

### hdfs-site.xml

    <?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
    <!-- Put site-specific property overrides in this file. -->
    <configuration>
        <property>
            <name>dfs.replication</name>
            <value>1</value>
        </property>
    </configuration>

### core-site.xml

    <?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
    <!-- Put site-specific property overrides in this file. -->
     <configuration>
       <property>
         <name>fs.defaultFS</name>
         <value>hdfs://localhost:9000</value>
       </property>
     </configuration>


### PATH

Make sure the PATH var conatains the $HADOOP_HOME/bin directory.


### create filesystem

    hdfs namenode -format

Filesystem is by default created under

    /tmp/hadoop-<user name>/

Path can be changed through the configuration files

### enable ssh

Enable password-less authentication and start sshd

On MacOS:

    System Preferences/Sharing/Remote Login


### run hadoop

Start everything:

    DEPRECATED: $HADOOP_HOME/sbin/start-all.sh

    $HADOOP_HOME/sbin/start-dfs.sh
    (optional with basic config) $HADOOP_HOME/sbin/start-yarn.sh
    
Stop:

    DEPRECATED:  $HADOOP_HOME/sbin/stop-all.sh

    $HADOOP_HOME/sbin/stop-dfs.sh
    (optional with basic config) $HADOOP_HOME/sbin/stop-yarn.sh

### start filesystem

    $HADOOP_HOME/sbin/start-dfs.sh


### copy data

Create directories:

    hdfs dfs -mkdir /user
    hdfs dfs -mkdir /user/ugo
    hdfs dfs -mkdir /user/ugo/input

Do not create `/user/ugo/output` directory; it will be created by Hadoop.
Hadoop startup fails if output directory already created.

Copy data (local to hdfs):

    hdfs dfs -put ./purchases-10k.txt /user/ugo/input/


### launch


Assuming you have successfully built the project with `mvn install` and are currently inside the `target` directory.

    yarn jar wordcount-1.0-SNAPSHOT.jar me.ugovaretto.WordCount  input output

### get results

    hdfs dfs -get /user/ugo/output
    cd output
    ...
    
### streaming

    $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.7.0.jar


    $HADOOP_HOME/bin/yarn jar \
    $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.7.0.jar \
    -input myInputDirs \
    -output myOutputDir \
    -mapper /bin/cat \
    -reducer /bin/wc
    -combiner .... (can use reducer)

Streaming environment:

access to streaming environment happens through environment variables;
for a complete list of variables look at

* [Mapper env vars](hadoop-streaming-env-vars-mapper.txt)
* [Reducer env vars](hadoop-streaming-env-vars-reducer.txt)


Running
-------

Look on the internet for directions to run single node or pseudo-cluster Hadoop.

Hadoop requires the following services to be started before it can operate:

    sbin/hadoop-daemon.sh start namenode

    sbin/hadoop-daemon.sh start datanode

    sbin/yarn-daemon.sh resourcemanager

Hadoop configuration directory is set by default to `$HADOOP_HOME/etc/hadoop` but can
be changed to anything by setting the `HADOOP_CONF_DIR` variable or by passing the
config dir to `hadoop-daemon(s).sh` or the other startup script through the
`--config <directory>` option.

*Note*: `HADOOP_HOME` is *probably* deprecated, use `HADOOP_PREFIX` instead. 

The main configuration files to edit are:

* `core-site.xml`
* `hdfs-site.xml`
* `masters`
* `slaves`

*Note*: Hadoop version >= 2 use a different systema to manage the cluster, including
a new scheduler/resource manager: yarn.

[Hadoop cluster config](http://solaimurugan.blogspot.ch/2013/11/setup-multi-node-hadoop-20-cluster.html)

Includes all the required information to properly configure a cluster.

Other
-----

The `wccl` directory contains a simple clojure application. Clojure can be used as the
language of choice instead of Java. Main advantage is much shorter and cleaner (lisp-like) code.


